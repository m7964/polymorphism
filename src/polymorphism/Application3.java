package polymorphism;

public class Application3 {
	
	public static void main(String[] args) {
		
		/* 매개변수에도 다형성 활용할수 있다. */		
		Application3 app3 = new Application3();
		
		Animal animal1 = new Rabbit();
		Animal animal2 = new Tiger();
		
		app3.feed(animal1);		// 동일한 타입이기 때문에 가능하다.
		app3.feed(animal2);
		
		
		Rabbit animal3 = new Rabbit();
		Tiger animal4 = new Tiger();
		
		app3.feed((Animal) animal3);	// 업캐스팅 명시적 형변환
		app3.feed((Animal) animal4);
		
		app3.feed(animal3);
		app3.feed(animal4);
	}
	
	
	/**
	 * <pre>
	 *   동물에게 먹이를 주기 위한 용도의 메소드 
	 * </pre>
	 * @param animal
	 */
	public void feed(Animal animal) {
		animal.eat();
	}
	
	
	
}


































