package polymorphism;

public class Tiger extends Animal{

	@Override
	public void eat() {
		
		System.out.println("호랑이가 고기를 뜯어 먹습니다.");
	}

	@Override
	public void run() {
		
		System.out.println("호랑이는 어슬렁~ 어슬렁~ 걷는다.");
	}

	@Override
	public void cry() {
		
		System.out.println("어흥~~~~~~~~ 어흥~~~~~~~~");
	}
	
	
	public void bite() {
		
		System.out.println("호랑이가 물어뜯습니다. 앙~");
	}

}


















