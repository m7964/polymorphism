package polymorphism;

public class Application {
	
	public static void main(String[] args) {
		
		System.out.println("Animal 생성 ===============================");		
		Animal animal = new Animal();
		animal.eat();
		animal.run();
		animal.cry();
		

		System.out.println("Rabbit 생성 ===============================");		
		Rabbit rabbit = new Rabbit();
		rabbit.eat();
		rabbit.run();
		rabbit.cry();
		rabbit.jump();
		
		
		System.out.println("Tiger 생성 ===============================");		
		Tiger tiger = new Tiger();
		tiger.eat();
		tiger.run();
		tiger.cry();
		tiger.bite();
		
		
		
		
		
		
		Animal a1 = new Rabbit();
		Animal a2 = new Tiger();
		
		System.out.println("동적 바인딩 확인 ====================================");
		a1.cry();
		a2.cry();
		
		
		/* 레퍼런스 변수는 Animal 이기 때문에 Rabbit과 Tigger 가 가지고 있는 고유한 기능을 동작시키기 못함.*/
//		a1.jump(); // 에러
//		a2.bite(); // 에러
		
		
		
		System.out.println("클래스타입의 형변환 확인 ====================================");
		((Rabbit) a1).jump();	// 다운캐스팅
		((Tiger) a2).bite();	// 다운캐스팅		class type casting: 클래스 형변환
		
		
		
		
		System.out.println("============ instanceof 확인 ==============");
		System.out.println("a1이 Tiger 타입인지 확인 : " + (a1 instanceof Tiger));
		System.out.println("a1이 Rabbit 타입인지 확인 : " + (a1 instanceof Rabbit));
		
		/* 상속받은 타입도 함께 가지고있다 ( 다형성 ) */
		System.out.println("a1이 Anumal 타입인지 확인 : " + (a1 instanceof Animal));
		System.out.println("a1이 Object 타입인지 확인 : " + (a1 instanceof Object));
		
		if( a1 instanceof Rabbit) {
			((Rabbit) a1).jump();
		}
		
		if(a2 instanceof Tiger) {
			((Tiger) a2).bite();
		}
		
		
		/* 클래스형변환은 up-casting과 down-casting으로 구분 할수 있다.*/
		/* up-casting : 자식이 부모타입으로 형변환 (상위 타입) 
		 * down-casting : 부모가 자식타입으로 형변환 (다운 캐스팅)
		 * */
		
		
		
		/* 명시적 형변환 */
		Animal animal1 = (Animal) new Rabbit();		// up-casting 명시적 형변환
		
		/* 묵시적 형변환*/
		Animal animal2 = new Rabbit();				// up-casting 묵시적 형변환
		
		Rabbit rabbit1 = (Rabbit) animal;			// donw-casting 명시적 형변환
//		Rabbit rabbit2 = animal2;					// down-casting 묵시적 형변환이 안된다.
		
		
		
	}

}




























